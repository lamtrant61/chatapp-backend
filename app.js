var http = require("http");
var express = require("express");
var cookieParser = require("cookie-parser");
const cors = require("cors");
const { NotFoundError } = require("./src/cores/ApiError");
const { handleError } = require("./src/middlewares/handleError");
const path = require("path");
const fs = require("fs");

const register = require("./src/routes/register.router");
const login = require("./src/routes/login.router");
const logout = require("./src/routes/logout.router");
const changePass = require("./src/routes/changePass.router");
const user = require("./src/routes/user.router");

var socketIo = require("./src/sockets/config");
require("./src/database/config");
require("./src/configs/autoRun");

var app = express();
var server = http.createServer(app);
socketIo(server);

app.use(
  cors({
    origin: "*",
    credentials: true,
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// app.use("/register", register);
// app.use("/login", login);
// app.use("/logout", logout);
// app.use("/changePass", changePass);
// app.use("/user", user);

const basePath = "/api/v1";
// Tự động tạo routing
let routeFiles = path.join(__dirname, "./src/routes");
fs.readdirSync(routeFiles)
  .filter((name) => {
    return name.toLowerCase().endsWith(".router.js");
  })
  .map((name) => name.replace(".js", ""))
  .forEach((name) => {
    const routeName = `./src/routes/${name}`;
    let routerPath =
      name === "index.router" ? "" : `/${name.replace(".router", "")}`;
    const urlPath = `${basePath}${routerPath}`;
    console.log(`Auto load: ${routeName} -> ${urlPath}`);
    app.use(urlPath, require(routeName));
  });

app.all("*", (req, res, next) => {
  next(new NotFoundError("Page not found"));
});
app.use(handleError);

module.exports = server;
