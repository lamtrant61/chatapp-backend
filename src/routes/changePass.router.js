const express = require("express");
const router = express.Router();
const changePassController = require("../controllers/changePass");
const validator = require("../middlewares/validator");
const changePassSchema = require("../schemas/changePass");
const authentication = require("../middlewares/authentication");
const { handleCookie } = require("../middlewares/handleCookie");

router.use(handleCookie);
router.use(authentication.handleBearerToken);

router.route("/").post(validator(changePassSchema), changePassController);

module.exports = router;
