const express = require("express");
const router = express.Router();
const validator = require("../middlewares/validator");
const groupSchema = require("../schemas/group");
const authentication = require("../middlewares/authentication");
const { handleCookie } = require("../middlewares/handleCookie");
const groupController = require("../controllers/group");

router.use(handleCookie);
router.use(authentication.handleBearerToken);

router
  .route("/")
  .get(groupController.readAll())
  .post(validator(groupSchema), groupController.create);
router.route("/:id").get(groupController.read());

module.exports = router;
