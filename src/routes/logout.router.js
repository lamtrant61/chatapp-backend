const express = require("express");
const router = express.Router();
const logoutController = require("../controllers/logout");
const validator = require("../middlewares/validator");
const logoutSchema = require("../schemas/logout");
const authentication = require("../middlewares/authentication");
const { handleCookie } = require("../middlewares/handleCookie");

router.use(handleCookie);
router.use(authentication.handleBearerToken);
router.route("/").post(validator(logoutSchema), logoutController.logout);

module.exports = router;
