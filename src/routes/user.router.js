const express = require("express");
const router = express.Router();
const authentication = require("../middlewares/authentication");
const { handleCookie } = require("../middlewares/handleCookie");
const {
  listFriendController,
  listRequestFriendController,
  searchUserController,
  addFriendController,
  acceptFriendController,
  rejectFriendController,
} = require("../controllers/user");

router.use(handleCookie);
router.use(authentication.handleBearerToken);

router.route("/listFriend").get(listFriendController);
router.route("/listRequestFriend").get(listRequestFriendController);

router.route("/searchUser/:username").get(searchUserController);
router.route("/addFriend/:username").post(addFriendController);

// This router is for the receiver to accept or reject the friend request
// :username is the sender's username
router.route("/acceptFriend/:username").post(acceptFriendController);
router.route("/rejectFriend/:username").post(rejectFriendController);

module.exports = router;
