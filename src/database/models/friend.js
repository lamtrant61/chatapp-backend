const mongoose = require("mongoose");

const FriendSchema = new mongoose.Schema({
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Users",
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Users",
  },
  status: {
    type: String,
    enum: ["pending", "accepted", "rejected"],
    default: "pending",
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updateAt: {
    type: Date,
    default: Date.now,
  },
});

const Friend = mongoose.model("Friend", FriendSchema);

module.exports = Friend;
