const mongoose = require("mongoose");

const GroupMemberSchema = new mongoose.Schema({
  groupId: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  permission: {
    type: String,
    required: true,
    default: "member",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updateAt: {
    type: Date,
    default: Date.now,
  },
});

const GroupMember = mongoose.model("GroupMember", GroupMemberSchema);

module.exports = GroupMember;
