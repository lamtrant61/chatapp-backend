require("dotenv").config();
const mongoose = require("mongoose");

// MongoDB connection URL
const dbURI = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
// const dbURI = `mongodb://${db.host}:${db.port}/${db.name}`;

mongoose
  .connect(dbURI, { replicaSet: "rs0", directConnection: true })
  .then(() => {
    console.log("Connected to MongoDB!");
  })
  .catch((err) => {
    console.error("Error connecting to MongoDB:", err);
  });
