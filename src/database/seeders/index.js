const mongoose = require("mongoose");
const seedUsers = require("./user");
require("dotenv").config();

async function seedDatabase() {
  console.log(process.env.DB_HOST);
  try {
    await mongoose.connect(
      `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }
    );

    await seedUsers();

    console.log("Database seeded successfully");
  } catch (error) {
    console.error("Error seeding database:", error);
  } finally {
    mongoose.connection.close();
  }
}

// Invoke the seed function
seedDatabase();
