const User = require("../models/user");
const { hash } = require("../../utils/bcrypt");
const { createToken } = require("../../utils/handleToken");

const users = [
  {
    username: "admin",
    name: "admin",
    email: "ngaybenta@gmail.com",
    password: "310898",
    role: "admin",
  },
  {
    username: "lamtt",
    name: "lamtt",
    email: "obelish01@gmail.com",
    password: "123456",
  },
];

async function handleUsers(users) {
  for (let i = 0; i < users.length; i++) {
    let refreshToken = createToken({
      tokenType: "refresh",
      username: users[i].username,
    });
    users[i].password = await hash(users[i].password);
    users[i].refreshToken = refreshToken;
  }
  return users;
}

async function seedUsers() {
  const allUsers = await handleUsers(users);
  try {
    await User.deleteMany();
    await User.insertMany(allUsers);
    console.log("User data seeded successfully");
  } catch (error) {
    console.error("Error seeding user data:", error);
  }
}

module.exports = seedUsers;
