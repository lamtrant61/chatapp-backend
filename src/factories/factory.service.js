module.exports = {
  readModel: async (Model, data) => {
    return await Model.find({ name: { $regex: data, $options: "i" } }).select(
      "-__v"
    );
  },
  readModelById: async (Model, id) => {
    return await Model.findOne({ id }).select("-__v");
  },
  createModel: async (Model, data) => {
    const newModel = new Model(data);
    return await newModel.save();
  },
  updateModel: async (Model, id, data) => {
    const model = await Model.findOne({ id });
    if (!model) return null;
    for (const key in data) {
      model[key] = data[key];
    }
    await model.save();
    return model;
  },
};
