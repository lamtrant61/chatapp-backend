const { handleControllerError } = require("../utils/handleControllerError");
const { SuccessResponse, SuccessMsgResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const factoryService = require("./factory.service");

class FactoryController {
  constructor(Model) {
    this.Model = Model;
  }
  read() {
    return handleControllerError(async (req, res, next) => {
      const { id } = req.params.id;
      let data = await factoryService.readModelById(this.Model, id);
      if (!data)
        return next(new BadRequestError(`${this.Model.name} not found`));
      return new SuccessResponse("Success", data).send(res);
    });
  }
  readAll() {
    return handleControllerError(async (req, res, next) => {
      const { name } = req.query;
      let data = await factoryService.readModel(this.Model, name);
      if (!data)
        return next(new BadRequestError(`${this.Model.name} not found`));
      return new SuccessResponse("Success", data).send(res);
    });
  }
  create() {
    return handleControllerError(async (req, res, next) => {
      let data = await factoryService.createModel(this.Model, req.data);
      if (!data)
        return next(new BadRequestError(`${this.Model.name} not found`));
      data = data.map((item) => item.dataValues);
      return new SuccessResponse("Success", data).send(res);
    });
  }
  update() {
    return handleControllerError(async (req, res, next) => {
      const dataUpdate = Object.keys(req.body).reduce((acc, item) => {
        if (
          item !== "id" &&
          item !== "withCredentials" &&
          item !== "refreshToken"
        )
          acc[item] = req.body[item];
        return acc;
      }, {});
      const t = await sequelize.transaction();
      req.transaction = t;
      let data = await factoryService.updateModel(
        this.Model,
        req.body.id,
        dataUpdate
      );
      if (!data) {
        await t.rollback();
        return next(new BadRequestError(`${this.Model.name} not found`));
      }
      await t.commit();
      return new SuccessMsgResponse("Success").send(res);
    });
  }
  delete() {
    return handleControllerError(async (req, res, next) => {
      const t = await sequelize.transaction();
      req.transaction = t;
      let data = await factoryService.deleteModel(this.Model, req.body.id);
      if (!data) {
        await t.rollback();
        return next(new BadRequestError(`${this.Model.name} not found`));
      }
      await t.commit();
      return new SuccessMsgResponse("Success").send(res);
    });
  }
}

module.exports = FactoryController;
