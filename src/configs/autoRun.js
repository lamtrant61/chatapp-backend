const { redisClient } = require("./redis");
require("dotenv").config();

const keyRedis = process.env.REDIS_USERS_ONLINE;

(async () => {
  const checkKeyExist = await redisClient.exists(keyRedis);
  if (checkKeyExist) {
    await redisClient.del(keyRedis);
  }
})();
