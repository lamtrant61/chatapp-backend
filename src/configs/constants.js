const ROLE = {
    'ADMIN': 0,
    'CUSTOMER': 1
}

module.exports = { ROLE }