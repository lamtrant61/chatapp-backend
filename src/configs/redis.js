const redis = require("redis");
require("dotenv").config();

// const redisClient = redis.createClient({
//   host: process.env.REDIS_HOST, // Redis server host
//   port: process.env.REDIS_PORT, // Redis server port
// });

const redisClient = redis.createClient({
  url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
});
redisClient.connect();
redisClient.on("connect", () => {
  console.log("Connected to Redis");
});

redisClient.on("error", (err) => {
  console.error("Error:", err);
});

module.exports = { redisClient };
