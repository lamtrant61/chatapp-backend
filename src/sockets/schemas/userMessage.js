const Joi = require("joi");

const userMessageSchema = Joi.object({
  receiverId: Joi.string().required(),
  message: Joi.string().required(),
});

module.exports = userMessageSchema;
