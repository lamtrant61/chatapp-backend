const userMessageSchema = require("../schemas/userMessage");
const Message = require("../../services/message");

const handleUserMessage = (socket, io) => {
  // This socket use to emit message from another user
  // Socket.userId is sender's id
  socket.on(`EmitPrivateMessage_${socket.userId}`, async (data) => {
    try {
      const { error } = userMessageSchema.validate(data);
      if (error)
        return io.to(socket.id).emit("Error", error.details[0].message);
      const newMessage = await Message.createNewMessage(
        socket.userId,
        data.receiverId,
        data.message
      );
      //console.log(newMessage);
      return io.emit(`ListenPrivateMessage_${data.receiverId}`, {
        senderId: newMessage.sender,
        message: newMessage.message,
        time: newMessage.createdAt,
      });
    } catch (error) {
      return io.to(socket.id).emit("Error", "Invalid error");
    }
  });

  // This socket use to listen message from userId
  // Socket.userId is receiver's id
  socket.on(`ListenPrivateMessage_${socket.userId}`, (data) => {});
};

module.exports = { handleUserMessage };
