const errorHandler = async (err, socket, next) => {
  socket.emit(`Error_${socket.clientId}`, err);
};
module.exports = { errorHandler };
