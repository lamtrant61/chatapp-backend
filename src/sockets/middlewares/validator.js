const userMessageSchema = require("../userMessage");

const validator = (schema) => (socket, next) => {
  const { error } = userMessageSchema.validate(data);
  if (error) return io.to(socket.id).emit("PrivateMessage", "Invalid data");
};

module.exports = validator;
