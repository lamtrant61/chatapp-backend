const socketIo = require("socket.io");
const cookie = require("cookie");
const { redisClient } = require("../configs/redis");
const { socketAuthenication } = require("./middlewares/socketAuthenication");
const { handleUserMessage } = require("./message/userMessage");
require("dotenv").config();
const keyRedis = process.env.REDIS_USERS_ONLINE;

const socket = (server) => {
  const io = socketIo(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
      credentials: true,
    },
  });
  io.use((socket, next) => {
    //console.log(cookie.parse(socket.request.headers.cookie));
    const cookieSocket = socket.request.headers.cookie || "";
    //const getCookie = cookie.parse(cookieSocket).userData;
    const getCookie = JSON.parse(cookieSocket).userData;
    //socket.userData = getCookie ? JSON.parse(getCookie) : null;
    socket.userData = getCookie ? getCookie : null;
    socket.clientIp = socket.request.connection.remoteAddress;
    socket.clientId = socket.id;
    next();
  });
  io.use(async (socket, next) => {
    await socketAuthenication(socket, next);
    // This must be assign userId in socket.userId
    console.log(socket.userData);
    socket.userId = socket.userData.id;
  });
  //handleUserMessage(socket, io);
  // io.use(async (socket, next) => {
  //   await validator(socket, next);
  // })

  io.on("connection", async (socket) => {
    const clientIp = socket.request.connection.remoteAddress;
    const clientId = socket.id;
    //socket.userData = JSON.parse(socket.userData);
    await redisClient.lPush(keyRedis, clientId);
    const allUsers = await redisClient.lRange(keyRedis, 0, -1);
    console.log("user data: ", socket.userData);
    socket.emit("allUsers", allUsers);
    //console.log(`${clientId} connected`);
    console.log(`PrivateMessage_${socket.userId}`);
    handleUserMessage(socket, io);
    socket.on("disconnect", async () => {
      console.log(`${clientId} disconnected`);
      await redisClient.lRem(keyRedis, 1, clientId);
      const allUsers = await redisClient.lRange(keyRedis, 0, -1);
      io.emit("allUsers", allUsers);
    });
  });
};

module.exports = socket;
