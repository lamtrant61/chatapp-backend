const { verifyToken, decodeAccessToken } = require("../utils/handleToken");
const { BadRequestError } = require("../cores/ApiError");
const User = require("../services/user");
const { createToken } = require("../utils/handleToken");
const { redisClient } = require("../configs/redis");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const extractBearerToken = (bearerHeader) => {
  const token = bearerHeader.startsWith("Bearer ")
    ? bearerHeader.split("Bearer ")[1]
    : "";
  return token;
};
const handleBearerToken = async (req, res, next) => {
  let refreshToken;
  let tokenInfo;
  try {
    refreshToken = req.body.refreshToken;
    tokenInfo = extractBearerToken(req.headers.authorization);
    if (!tokenInfo) {
      return next(new BadRequestError("Invalid bearer token"));
    }

    if (await redisClient.exists(tokenInfo)) {
      return next(new BadRequestError("Token has been revoked"));
    }
    const decoded = await verifyToken({
      token: tokenInfo,
      tokenType: "access",
    });
    const user = await User.getUserInfoByUsername(decoded.username);
    if (!user) {
      return next(new BadRequestError("User not found"));
    }

    req.user = decoded;
    req.username = decoded.username;
    next();
  } catch (error) {
    if (error instanceof jwt.TokenExpiredError) {
      try {
        const username = (await decodeAccessToken(tokenInfo)).username;
        const user = await User.getUserInfoByUsername(username);
        if (refreshToken === user.dataValues.Token.dataValues.token) {
          await verifyToken({
            token: refreshToken,
            tokenType: "refresh",
          });
          const newAccessToken = createToken({
            username: username,
            tokenType: "access",
          });
          req.username = username;
          await redisClient.set(tokenInfo, "revoked", {
            EX: parseInt(process.env.REDIS_EXPIRE),
          });
          res.cookie("newAccessToken", newAccessToken, {
            expires: new Date(Date.now() + 10000),
          });
          return next();
        }
        return next(new BadRequestError("Token has expired"));
      } catch (error) {
        return next(new BadRequestError("Invalid token"));
      }
    }
    return next(new BadRequestError("Invalid token"));
  }
}

module.exports = {
  extractBearerToken,
  handleBearerToken,
};
