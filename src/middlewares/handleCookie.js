const { BadRequestError } = require("../cores/ApiError");

module.exports = {
  handleCookie: async (req, res, next) => {
    try {
      const { refreshToken } = req.cookies;
      req.body.refreshToken = refreshToken;
      next();
    } catch (error) {
      return next(new BadRequestError("Invalid refresh token"));
    }
  },
};
