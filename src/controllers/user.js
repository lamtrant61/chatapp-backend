const { SuccessResponse, SuccessMsgResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { handleControllerError } = require("../utils/handleControllerError");
const User = require("../services/user");
const Friend = require("../services/friend");

const searchUserController = handleControllerError(async (req, res, next) => {
  const { username } = req.params;
  const searchAllUser = await User.searchAllUserByUsername(
    req.username,
    username
  );
  if (!searchAllUser) return next(new BadRequestError(searchAllUser.message));
  const userData = searchAllUser.reduce((acc, cur) => {
    acc.push(cur.username);
    return acc;
  }, []);
  return new SuccessResponse("Success", userData).send(res);
});

const listRequestFriendController = handleControllerError(async (req, res, next) => {
  const user = await User.getUserInfoByUsername(req.username)
  if (!user) return next(new BadRequestError("User not found"));
  const listRequest = await Friend.listRequestFriend(user.id)
  return new SuccessResponse("Success", listRequest.data).send(res);
});

const listFriendController = handleControllerError(async (req, res, next) => {
  const user = await User.getUserInfoByUsername(req.username)
  if (!user) return next(new BadRequestError("User not found"));
  return new SuccessResponse("Success", user.friends).send(res);
});

const addFriendController = handleControllerError(async (req, res, next) => {
  const { username } = req.params;
  const sender = await User.getUserInfoByUsername(req.username);
  const receiver = await User.getUserInfoByUsername(username);
  if (!sender || !receiver) return next(new BadRequestError("User not found"));

  const requestFriend = await Friend.requestFriend(sender.id, receiver.id);
  if (!requestFriend.data)
    return next(new BadRequestError(requestFriend.message));
  return new SuccessMsgResponse("Success").send(res);
});

const acceptFriendController = handleControllerError(async (req, res, next) => {
  const { username } = req.params;
  const sender = await User.getUserInfoByUsername(username);
  const receiver = await User.getUserInfoByUsername(req.username);
  if (!sender || !receiver) return next(new BadRequestError("User not found"));
  const acceptFriendRequest = await Friend.acceptFriendRequest(
    sender.id,
    receiver.id
  );
  if (!acceptFriendRequest.data)
    return next(new BadRequestError(acceptFriendRequest.message));
  return new SuccessMsgResponse("Success").send(res);
});

const rejectFriendController = handleControllerError(async (req, res, next) => {
  const { username } = req.params;
  const sender = await User.getUserInfoByUsername(username);
  const receiver = await User.getUserInfoByUsername(req.username);
  if (!sender || !receiver) return next(new BadRequestError("User not found"));
  const rejectFriendRequest = await Friend.rejectFriendRequest(
    sender.id,
    receiver.id
  );
  if (!rejectFriendRequest.data)
    return next(new BadRequestError(rejectFriendRequest.message));
  return new SuccessMsgResponse("Success").send(res);
});

module.exports = {
  listFriendController,
  listRequestFriendController,
  searchUserController,
  addFriendController,
  acceptFriendController,
  rejectFriendController,
};
