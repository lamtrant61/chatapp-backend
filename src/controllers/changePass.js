const { SuccessMsgResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { handleControllerError } = require("../utils/handleControllerError");
const User = require("../services/user");

const changePassController = handleControllerError(async (req, res, next) => {
  const { oldPassword, newPassword, retypeNewPassword } = req.body;
  if (newPassword !== retypeNewPassword) {
    return next(new BadRequestError("Retype password is not match"));
  }
  const changePasswordData = await User.changePassword(
    req.username,
    oldPassword,
    newPassword
  );
  if (!changePasswordData?.data)
    return next(new BadRequestError(changePasswordData.message));
  return new SuccessMsgResponse("Success").send(res);
});
module.exports = changePassController;
