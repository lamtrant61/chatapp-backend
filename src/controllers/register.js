const { handleControllerError } = require("../utils/handleControllerError");
const { createToken } = require("../utils/handleToken");
const { SuccessResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const User = require("../services/user");

const register = handleControllerError(async (req, res, next) => {
  const { name, username, password, email } = req.body;
  const newUser = new User({ name, username, email });
  req.transaction = newUser;

  const checkUser = await newUser.findUserByUsername();
  if (checkUser) return next(new BadRequestError("User already exists"));

  await newUser.initialize();
  const result = await newUser.createUser(password);
  await newUser.commit();

  const accessToken = createToken({
    tokenType: "access",
    username: username,
  });

  return new SuccessResponse("Register success", {
    name: result.name,
    username: result.username,
    email: result.email,
    accessToken,
    refreshToken: result.refreshToken,
  }).send(res);
});

module.exports = {
  register,
};
