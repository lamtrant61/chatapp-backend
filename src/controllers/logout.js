const { handleControllerError } = require("../utils/handleControllerError");
const { SuccessResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { redisClient } = require("../configs/redis");
const User = require("../services/user");
require("dotenv").config();

const logout = handleControllerError(async (req, res, next) => {
  const { refreshToken } = req.body;
  const accessToken = req.headers.authorization.split("Bearer ")[1];
  const logoutData = await User.getLogout(req.username, refreshToken);
  if (!logoutData?.data) return next(new BadRequestError(logoutData.message));
  await redisClient.set(accessToken, "revoked", {
    EX: parseInt(process.env.REDIS_EXPIRE),
  });

  //let key = await redisClient.sendCommand(["KEYS", "*"]);
  //let data = await redisClient.sendCommand(["GET", key[0]]);
  return new SuccessResponse("Success", {
    message: "Logout success",
  }).send(res);
});
module.exports = {
  logout,
};
