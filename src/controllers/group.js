const { SuccessResponse, SuccessMsgResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { handleControllerError } = require("../utils/handleControllerError");
const FactoryController = require("../factories/factory.controller");
const GroupSchema = require("../database/models/group");
const Group = require("../services/group");
const factoryService = require("../factories/factory.service");

const GroupController = new FactoryController(GroupSchema);
GroupController.create = handleControllerError(async (req, res, next) => {
  let data = await factoryService.createModel(GroupSchema, req.body);
  if (!data) return next(new BadRequestError(`${GroupSchema.name} not found`));
  //console.log(data);
  return new SuccessResponse("Success", data).send(res);
});

module.exports = GroupController;
