const { handleControllerError } = require("../utils/handleControllerError");
const { SuccessResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const User = require("../services/user");

const login = handleControllerError(async (req, res, next) => {
  const { username, password } = req.body;
  const loginData = await User.getLogin(username, password);
  if (!loginData?.data) return next(new BadRequestError(loginData.message));
  return new SuccessResponse("Login success", loginData.data).send(res);
});
module.exports = {
  login,
};
