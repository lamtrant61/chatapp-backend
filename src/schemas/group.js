const Joi = require("joi");

const groupSchema = Joi.object({
  name: Joi.string().min(3).max(50).required(),
  description: Joi.string().min(3).max(200).required(),
  refreshToken: Joi.string().min(50).max(200),
});

module.exports = groupSchema;
