const Joi = require("joi");

const logoutSchema = Joi.object({
  refreshToken: Joi.string().min(50).max(200).required(),
});

module.exports = logoutSchema;
