const Model = require("./model");
const UserSchema = require("../database/models/user");
const { hash, compare } = require("../utils/bcrypt");
const { createToken } = require("../utils/handleToken");

class User extends Model {
  constructor({ username, name, email, role }) {
    super();
    this.username = username;
    this.name = name;
    this.email = email;
    this.role = role || "user";
  }
  async createUser(password) {
    const refreshToken = createToken({
      tokenType: "refresh",
      username: this.username,
    });
    const hashPassword = await hash(password);
    const user = new UserSchema({
      username: this.username,
      name: this.name,
      email: this.email,
      password: hashPassword,
      refreshToken: refreshToken,
      role: this.role,
    });
    const result = await user.save();
    return result;
  }
  static async getLogin(username, password) {
    const checkUser = await UserSchema.findOne({ username: username });
    if (!checkUser) return { message: "User not found" };
    const matchPassword = await compare(password, checkUser.password);
    if (!matchPassword) return { message: "Password is incorrect" };
    const accessToken = createToken({
      tokenType: "access",
      username: username,
    });
    const refreshToken = createToken({
      tokenType: "refresh",
      username: username,
    });
    await UserSchema.findOneAndUpdate(
      { username: username },
      { refreshToken: refreshToken }
    );
    return {
      message: "Login success",
      data: { username, accessToken, refreshToken },
    };
  }
  static async getLogout(username, refreshToken) {
    const checkUser = await UserSchema.findOne({ username: username });
    if (!checkUser) return { message: "User not found" };
    if (checkUser.refreshToken !== refreshToken)
      return { message: "Refresh token is incorrect" };
    await UserSchema.findOneAndUpdate(
      { username: username },
      { refreshToken: null }
    );
    return {
      message: "Logout success",
      data: { username, status: "Ok" },
    };
  }
  static async changePassword(username, oldPassword, newPassword) {
    const checkUser = await UserSchema.findOne({ username: username });
    if (!checkUser) return { message: "User not found" };
    const matchPassword = await compare(oldPassword, checkUser.password);
    if (!matchPassword) return { message: "Old password is incorrect" };
    const hashPassword = await hash(newPassword);
    await UserSchema.findOneAndUpdate(
      { username: username },
      { password: hashPassword }
    );
    return {
      message: "Change password success",
      data: { username, status: "Ok" },
    };
  }
  async findUserByUsername() {
    return await UserSchema.findOne({ username: this.username });
  }
  static async getUserInfoByUsername(username) {
    return await UserSchema.findOne({ username: username }).select("-__v");
  }
  static async searchAllUserByUsername(selfUser, username) {
    return await UserSchema.find({
      $and: [
        { username: { $ne: selfUser } },
        { username: { $regex: username, $options: "i" } },
      ],
    }).select("-__v");
  }
}

module.exports = User;
