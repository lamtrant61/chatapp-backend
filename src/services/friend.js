const Model = require("./model");
const UserSchema = require("../database/models/user");
const FriendSchema = require("../database/models/friend");

class Friend extends Model {
  constructor() {
    super();
  }
  static async listRequestFriend(username) {
    const listRequest = await FriendSchema.find({
      receiver: username,
      status: "pending"
    }).select("-__v");
    return {
      message: "Request friend success",
      data: listRequest
    };
  }
  static async requestFriend(senderId, receiverId) {
    const acceptedRequest = await FriendSchema.findOne({
      $or: [
        { sender: senderId, receiver: receiverId, status: "accepted" },
        { sender: receiverId, receiver: senderId, status: "accepted" },
      ],
    });
    if (acceptedRequest) return { message: "Friend request already accepted" };

    const existingRequest = await FriendSchema.findOne({
      $or: [
        { sender: senderId, receiver: receiverId, status: "pending" },
        { sender: receiverId, receiver: senderId, status: "pending" },
      ],
    });
    if (existingRequest) return { message: "Friend request already sent" };
    const friend = new FriendSchema({
      sender: senderId,
      receiver: receiverId,
    });
    const result = await friend.save();
    return {
      message: "Request friend success",
      data: result,
    };
  }
  static async acceptFriendRequest(senderId, receiverId) {
    const existingRequest = await FriendSchema.findOne({
      $or: [{ sender: senderId, receiver: receiverId, status: "pending" }],
    });
    if (!existingRequest) return { message: "Friend request not found" };
    const sender = await UserSchema.findById(senderId);
    const receiver = await UserSchema.findById(receiverId);
    if (!sender || !receiver) return { message: "User not found" };
    sender.friends.push(receiverId);
    receiver.friends.push(senderId);
    existingRequest.status = "accepted";
    await existingRequest.save();
    await sender.save();
    await receiver.save();
    return { message: "Success", data: existingRequest };
  }
  static async rejectFriendRequest(senderId, receiverId) {
    const existingRequest = await FriendSchema.findOne({
      $or: [{ sender: senderId, receiver: receiverId, status: "pending" }],
    });
    if (!existingRequest) return { message: "Friend request not found" };
    existingRequest.status = "rejected";
    await existingRequest.save();
    return { message: "Success", data: existingRequest };
  }
}

module.exports = Friend;
