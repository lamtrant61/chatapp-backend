const Model = require("./model");
const MessageSchema = require("../database/models/message");

class Message extends Model {
  constructor() {
    super();
  }
  static async createNewMessage(senderId, receiverId, message) {
    const newMessage = new MessageSchema({
      sender: senderId,
      receiver: receiverId,
      message: message,
    });

    const result = await newMessage.save();
    return result;
  }
}

module.exports = Message;
