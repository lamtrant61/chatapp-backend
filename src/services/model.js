const mongoose = require("mongoose");

class Model {
  async initialize() {
    this.session = await mongoose.startSession();
    this.session.startTransaction();
  }
  async commit() {
    await this.session.commitTransaction();
    this.session.endSession();
  }
  async rollback() {
    await this.session.abortTransaction();
    this.session.endSession();
  }
}

module.exports = Model;
