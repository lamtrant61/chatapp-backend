const Model = require("./model");
const GroupSchema = require("../database/models/user");

class Group extends Model {
  constructor() {
    super();
  }
  static async searchAllGroup(groupName) {
    const listRequest = await GroupSchema.find({
      name: groupName,
    }).select("-__v");
    return {
      message: "Search all groups success",
      data: listRequest,
    };
  }
  static async searchGroupById(id) {
    const listRequest = await GroupSchema.findOne({
      id,
    }).select("-__v");
    return {
      message: "Search group by id success",
      data: listRequest,
    };
  }
  static async createNewGroup(data) {
    const newGroup = new GroupSchema({
      data,
    });
    const result = await newGroup.save();
    return {
      message: "Create new group success",
      data: result,
    };
  }
}

module.exports = Group;
