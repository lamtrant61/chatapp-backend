const handleControllerError = (fn) => {
  return (req, res, next) => {
    Promise.resolve(fn(req, res, next)).catch((err) => {
      if (req.transaction) {
        req.transaction.rollback();
      }
      next(err);
    });
  };
};

const handleSocketError = (fn) => {
  return (socket, next) => {
    Promise.resolve(fn(socket, next)).catch((err) => {
      next(err);
    });
  };
};

module.exports = { handleControllerError, handleSocketError };
