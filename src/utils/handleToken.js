const jwt = require("jsonwebtoken");
require("dotenv").config();

function getInfo(tokenType) {
  let expiresIn;
  let tokenKey;
  if (tokenType === "access") {
    expiresIn = process.env.ACCESS_TOKEN_EXPIRE;
    tokenKey = process.env.ACCESS_KEY;
  } else {
    expiresIn = process.env.REFRESH_TOKEN_EXPIRE;
    tokenKey = process.env.REFRESH_KEY;
  }
  return { expiresIn, tokenKey };
}

module.exports = {
  createToken: (payload) => {
    let { expiresIn, tokenKey } = getInfo(payload.tokenType);
    let options = { algorithm: "HS256", expiresIn: expiresIn };
    return jwt.sign(payload, tokenKey, options);
  },
  verifyToken: (payload) => {
    let { tokenKey } = getInfo(payload.tokenType);
    return jwt.verify(payload.token, tokenKey);
  },
  decodeAccessToken: (token) => {
    return jwt.decode(token, process.env.ACCESS_KEY);
  },
};
